const Db = require('mongodb').Db;
const Connection = require('mongodb').MongoClient;
const Server = require('mongodb').Server;
const assert = require('assert');
//the MongoDB connection
let connectionInstance;

module.exports = function(callback) {
  //if already we have a connection, don't connect to database again
  if (connectionInstance) {
    callback(connectionInstance);
    return;
  }

  Connection.connect(process.env.MONGODB_URI || `mongodb://mongodb:${process.env.PORT || 27017}`, function(err, client) {
    assert.equal(null, err);
    console.log("Connected successfully to server");
  
    const db = client.db('mais-barato-search');
    connectionInstance = db;
    callback(db);
  });
};