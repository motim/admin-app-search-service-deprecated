const admin = require("firebase-admin");
const serviceAccount = require("./supermarkets-products-index-firebase-adminsdk-jnupe-800b1722ce.json");
const mongo = require("./db.js");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://supermarkets-products-index.firebaseio.com"
});
const fb = admin.firestore();

const express = require('express');
const app = express();

const collectionName = 'products';
// create text search index to products collection
mongo(function(databaseConnection) {
    const productsCol = databaseConnection.collection(collectionName);
    productsCol.dropIndexes((err, res) => {
        productsCol.ensureIndex({ 'descricao': 'text' });
    });
});

// watch 'products' collectioon from fb and insert or update into mongo
const query = fb.collection(collectionName);
const observer = query.onSnapshot(
    querySnapshot => {
        mongo(function(databaseConnection) {
            const productsCol = databaseConnection.collection(collectionName);
            querySnapshot.docChanges.forEach(doc => {
                const data = doc.doc.data();
                data['_id'] = doc.doc.id;

                productsCol.update({_id: doc.doc.id}, {$set: data}, { upsert: true });
            });
        });
    },
    err => {
        console.log(`Encountered error: ${err}`);
    }
);

app.get('/', (req, res) => {
    res.send('alive');
});

app.get('/search', (req, res, next) => {
    const q = req.query.q;
    mongo(function(databaseConnection) {
        const productsCol = databaseConnection.collection(collectionName);
        productsCol.find({descricao: new RegExp(q, 'i')})
            .toArray((err, docs) => {
                if(!err){
                    res.send({'results': docs});
                }else {
                    return next(err);
                }
            });
    });
});

// handle error
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.listen(5002);